package com.rjz.sharareh.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btn0,
            btnclear, btnbs, btnequal, btnmult, btndiv, btnsub, btnadd, btnsign, btndecimal;
    TextView result, savenum;
    double num1 = 0;
    int sw = 0,clicked = 1;
    double resultsho = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        result = (TextView) findViewById(R.id.main_result);
        savenum = (TextView) findViewById(R.id.main_savenum);
        btn0 = (Button) findViewById(R.id.main_btnZero);
        btn1 = (Button) findViewById(R.id.main_btnOne);
        btn2 = (Button) findViewById(R.id.main_btnTwo);
        btn3 = (Button) findViewById(R.id.main_btnThree);
        btn4 = (Button) findViewById(R.id.main_btnFour);
        btn5 = (Button) findViewById(R.id.main_btnFive);
        btn6 = (Button) findViewById(R.id.main_btnSix);
        btn7 = (Button) findViewById(R.id.main_btnSeven);
        btn8 = (Button) findViewById(R.id.main_btnEight);
        btn9 = (Button) findViewById(R.id.main_btnNine);
        btnequal = (Button) findViewById(R.id.main_btnEquals);
        btnmult = (Button) findViewById(R.id.main_btnMultiply);
        btndiv = (Button) findViewById(R.id.main_btnDivide);
        btnsub = (Button) findViewById(R.id.main_btnSubtract);
        btnadd = (Button) findViewById(R.id.main_btnAdd);
        btnsign = (Button) findViewById(R.id.main_btnsign);
        btndecimal = (Button) findViewById(R.id.main_btnDecimal);
        btnclear = (Button) findViewById(R.id.main_btnClear);
        btnbs = (Button) findViewById(R.id.main_btnbs);

        //--------------------------------------------------------

        btn0.setOnClickListener(this);
        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
        btn5.setOnClickListener(this);
        btn6.setOnClickListener(this);
        btn7.setOnClickListener(this);
        btn8.setOnClickListener(this);
        btn9.setOnClickListener(this);
        btnequal.setOnClickListener(this);
        btndecimal.setOnClickListener(this);
        btndiv.setOnClickListener(this);
        btnadd.setOnClickListener(this);
        btnsub.setOnClickListener(this);
        btnmult.setOnClickListener(this);
        btnclear.setOnClickListener(this);
        btnsign.setOnClickListener(this);
        btnbs.setOnClickListener(this);
        //----------------------------------------------------------


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.main_btnOne:
                show("1");
                break;
            case R.id.main_btnTwo:
                show("2");
                break;
            case R.id.main_btnThree:
                show("3");
                break;
            case R.id.main_btnFour:
                show("4");
                break;
            case R.id.main_btnFive:
                show("5");
                break;
            case R.id.main_btnSix:
                show("6");
                break;
            case R.id.main_btnSeven:
                show("7");
                break;
            case R.id.main_btnEight:
                show("8");
                break;
            case R.id.main_btnNine:
                show("9");
                break;
            case R.id.main_btnZero:
                show("0");
                break;
            case R.id.main_btnbs:
                backspace();
                break;
            case R.id.main_btnClear:
                clear();
                break;
            case R.id.main_btnsign:
                sign();
                break;
            case R.id.main_btnDivide:
                divideready();
                break;
            case R.id.main_btnMultiply:
                multready();
                break;
            case R.id.main_btnSubtract:
                subready();
                break;
            case R.id.main_btnAdd:
                plusReady();
                break;
            case R.id.main_btnEquals:
                calculate();
                break;
            case R.id.main_btnDecimal:
                decimal();
                break;
        }


    }

    //-----------equal btn-----------------------------------------
    private void calculate() {

        double num2 = Double.parseDouble(result.getText().toString());


            savenum.setText(savenum.getText() + String.valueOf(decimalremove(num2)));



        //=====================================
        switch (sw) {
            case 1:
                resultsho = plus(num1, num2);
                show(String.valueOf(decimalremove(resultsho)), true);
                break;
            case 2:
                resultsho = sub(num1, num2);
                show(String.valueOf(decimalremove(resultsho)), true);
                break;
            case 3:
                resultsho = div(num1, num2);
                show(String.valueOf(decimalremove(resultsho)), true);
                break;
            case 4:
                resultsho = mult(num1, num2);
                show(String.valueOf(decimalremove(resultsho)), true);
                break;
        }
        clicked++;


    }

    //-------------operation function-------------------------------
    private double plus(double a, double b) {
        return a + b;
    }

    private double sub(double a, double b) {

        return (a - b);
    }

    private double div(double a, double b) {
        return a / b;
    }

    private double mult(double a, double b) {
        return a * b;
    }

    //--------------------------------------------------------------------
    private void plusReady() {
        sw = 1;
        num1 = Double.parseDouble(result.getText().toString());
        savenum.append(String.valueOf(decimalremove(num1)));
        result.setText("0");
        savenum.append("+");
    }

    private void subready() {
        sw = 2;
        num1 = Double.parseDouble(result.getText().toString());
        savenum.append(String.valueOf(decimalremove(num1)));
        result.setText("0");
        savenum.append("-");
    }

    private void divideready() {
        sw = 3;
        num1 = Double.parseDouble(result.getText().toString());
        savenum.append(String.valueOf(decimalremove(num1)));
        result.setText("0");
        savenum.append("/");
    }

    private void multready() {
        sw = 4;
        num1 = Double.parseDouble(result.getText().toString());
        savenum.append(String.valueOf(decimalremove(num1)));
        result.setText("0");
        savenum.append("*");
    }

    //------------------------------------------------------------------
    private void show(String n, Boolean force) {
        if (force) {
            result.setText(n);
        } else {
            String current = result.getText().toString();
            if (current.equals("0")) {
                result.setText(n);
            } else {
                result.append(n);
            }
        }
    }

    private void show(String n) {
        String current = result.getText().toString();
        if (current.equals("0")) {
            result.setText(n);
        } else {
            result.append(n);
        }
    }

    //--------------------------------------------------------------
    private String decimalremove(double numd) {
        DecimalFormat df = new DecimalFormat("###.#");
        String numre = df.format(numd);
        return numre;
    }

    //--------------------------------------------------------------------
    private void backspace() {
        if (result.getText().length() != 0) {
            String backchar = result.getText().toString();
            if (backchar.length() >= 1) {
                backchar = backchar.substring(0, backchar.length() - 1);
                result.setText(backchar);
            }
        } else
            result.setText("0");
    }

    //--------------------------------------------------------------------
    private void clear() {
        result.setText("");
        num1 = 0;
        savenum.setText("");
        btnequal.setEnabled(true);
    }

    //---------------------------------------------------------------------
    private void sign() {
        if (result.equals("0")) {
            result.setText("");
            result.setText("-");
        } else {
            String current = result.getText().toString();
            result.setText("-");
            result.append(current);
            result.append("");
        }
    }

    //---------------------------------------------------------------------
    private void decimal() {
        if (result.equals("") || result.equals("0")) {
            result.setText("0.");
        } else {
            result.append(".");
        }
    }
}
